package org.octopus.data.desensitized.jackson.builder;

import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.builder.DataDesensitizedToStringBuilder;
import org.octopus.data.desensitized.jackson.util.JacksonDesensitizedUtils;

/**
 * @author houjuntao
 * @date 2021/9/4 16:19
 */
public class JacksonDataDesensitizedToStringBuilder extends DataDesensitizedToStringBuilder {

    /**
     * @param object
     */
    public JacksonDataDesensitizedToStringBuilder(Object object) {
        super(object);
    }

    /**
     * @param object
     * @param style
     */
    public JacksonDataDesensitizedToStringBuilder(Object object, ToStringStyle style) {
        super(object, style);
    }

    /**
     * @param object
     * @param excludeNullValues
     * @param style
     */
    public JacksonDataDesensitizedToStringBuilder(Object object, boolean excludeNullValues, ToStringStyle style) {
        super(object, excludeNullValues, style);
    }

    /**
     * 获取字段的脱敏规则
     *
     * @param field
     * @return
     */
    @Override
    protected DesensitizedRule obtainDesensitized(Field field) {
        DesensitizedRule desensitizedRule = JacksonDesensitizedUtils.obtainDesensitizedRule(field);

        if (desensitizedRule == null) {
            return super.obtainDesensitized(field);
        }

        return desensitizedRule;
    }
}
