package org.octopus.data.desensitized.jackson.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.octopus.data.desensitized.core.registry.DesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.registry.GlobalDesensitizedRuleRegistry;
import org.octopus.data.desensitized.core.registry.impl.ServiceLoaderDesensitizedStrategyRegistry;

/**
 * @author lemon
 * @date 2021/8/28 10:07
 */
public class JacksonDesensitizedSerializerTest {

    public static void main(String[] args) throws JsonProcessingException {
        DesensitizedStrategyRegistry desensitizedStrategyRegistry = new ServiceLoaderDesensitizedStrategyRegistry();
        GlobalDesensitizedRuleRegistry globalDesensitizedRuleRegistry = new GlobalDesensitizedRuleRegistry();

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setAddress("BaseResponse-setAddress");
        baseResponse.setPhone("BaseResponse-setPhone");

        System.out.println("toString脱敏数据 " + baseResponse);

        System.out.println();
        System.out.println();
        System.out.println();

        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(objectMapper.writeValueAsString(baseResponse));

    }
}