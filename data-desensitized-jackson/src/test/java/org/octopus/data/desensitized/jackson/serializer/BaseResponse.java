package org.octopus.data.desensitized.jackson.serializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.octopus.data.desensitized.core.annotation.Desensitized;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;
import org.octopus.data.desensitized.jackson.annotation.JacksonDesensitized;
import org.octopus.data.desensitized.jackson.builder.JacksonDataDesensitizedToStringBuilder;

@Getter
@Setter
public class BaseResponse<T> {

    @JsonSerialize(using = JacksonDesensitizedSerializer.class)
    @Desensitized(desensitizedType = DesensitizedType.ADDRESS)
    private String address;

    @JacksonDesensitized(replacePrefixIgnoreLength = 2, replaceSuffixIgnoreLength = 3, replaceMaskSymbol = "$")
    private String phone;

    private T data;

    public BaseResponse() {
    }

    @Override
    public String toString() {
        return new JacksonDataDesensitizedToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}