package org.octopus.data.desensitized.core.strategy.impl;

import java.io.Serializable;
import lombok.ToString;
import org.octopus.data.desensitized.core.annotation.Desensitized;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

@ToString
public class AddressBO implements Serializable {

    private static final long serialVersionUID = 1L;

//	private String addrId;

    @Desensitized(desensitizedType = DesensitizedType.MOBILE_PHONE)
    private String addrName;

    private SubAddressBO subAddressBO;

    public AddressBO() {
    }

//	public AddressBO(String addrId, String addrName) {
//		super();
//		this.addrId = addrId;
//		this.addrName = addrName;
//	}
//
//	public String getAddrId() {
//		return addrId;
//	}
//
//	public void setAddrId(String addrId) {
//		this.addrId = addrId;
//	}

    public String getAddrName() {
        return addrName;
    }

    public void setAddrName(String addrName) {
        this.addrName = addrName;
    }

    public SubAddressBO getSubAddressBO() {
        return subAddressBO;
    }

    public void setSubAddressBO(SubAddressBO subAddressBO) {
        this.subAddressBO = subAddressBO;
    }
}
 