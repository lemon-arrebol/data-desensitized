package org.octopus.data.desensitized.core.strategy.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.ToString;
import org.octopus.data.desensitized.core.annotation.Desensitized;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

@ToString
public class UserBO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @Desensitized(desensitizedType = DesensitizedType.CHINESE_NAME)
    private String userName;

    private int age;

    private AddressBO address;

    private List<AddressBO> list = new ArrayList<>();

    private Map<String, AddressBO> map = new HashMap<>();
//	private Date date;
//
//	public Date getDate() {
//		return date;
//	}
//
//	public void setDate(Date date) {
//		this.date = date;
//	}

    private byte b;
    private short s;
    private boolean bool;
    private double dble;
    private Byte bb;
    private Short ss;
    private Integer integer;
    private Long aLong;
    private Double dd;
    private Boolean aBoolean;
    private AddressBO[] addressArr;

    public UserBO() {
    }

    public UserBO(String id, String userName, int age, AddressBO address) {
        super();
        this.id = id;
        this.userName = userName;
        this.age = age;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public AddressBO getAddress() {
        return address;
    }

    public void setAddress(AddressBO address) {
        this.address = address;
    }

    public List<AddressBO> getList() {
        return list;
    }

    public void setList(List<AddressBO> list) {
        this.list = list;
    }

    public Map<String, AddressBO> getMap() {
        return map;
    }

    public void setMap(Map<String, AddressBO> map) {
        this.map = map;
    }

    public byte getB() {
        return b;
    }

    public void setB(byte b) {
        this.b = b;
    }

    public short getS() {
        return s;
    }

    public void setS(short s) {
        this.s = s;
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public double getDble() {
        return dble;
    }

    public void setDble(double dble) {
        this.dble = dble;
    }

    public Byte getBb() {
        return bb;
    }

    public void setBb(Byte bb) {
        this.bb = bb;
    }

    public Short getSs() {
        return ss;
    }

    public void setSs(Short ss) {
        this.ss = ss;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public Long getaLong() {
        return aLong;
    }

    public void setaLong(Long aLong) {
        this.aLong = aLong;
    }

    public Double getDd() {
        return dd;
    }

    public void setDd(Double dd) {
        this.dd = dd;
    }

    public Boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

//	@Override
//	public String toString() {
//		return "UserVo [id=" + id + ", userName=" + userName + ", age=" + age + ", address=[addrId=" + address.getAddrId() + ",addrName=" + address.getAddrName() + "] ]";
//	}


    public AddressBO[] getAddressArr() {
        return addressArr;
    }

    public void setAddressArr(AddressBO[] addressArr) {
        this.addressArr = addressArr;
    }
}