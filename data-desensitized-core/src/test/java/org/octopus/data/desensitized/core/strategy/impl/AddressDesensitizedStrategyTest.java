package org.octopus.data.desensitized.core.strategy.impl;

import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.processor.DesensitizedProcessor;
import org.octopus.data.desensitized.core.processor.impl.DefaultDesensitizedProcessor;
import org.octopus.data.desensitized.core.registry.DesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.registry.impl.ServiceLoaderDesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.strategy.DesensitizedStrategy;
import org.octopus.data.desensitized.core.util.DesensitizedUtils;

/**
 * @author lemon
 * @date 2021/8/27 14:33
 */
public class AddressDesensitizedStrategyTest {

    public static void main(String[] args) {
        DesensitizedStrategyRegistry desensitizedStrategyRegistry = new ServiceLoaderDesensitizedStrategyRegistry();

        DesensitizedStrategy<String> desensitizedStrategy =
                desensitizedStrategyRegistry
                        .obtainDesensitizedStrategy(String.class, DesensitizedPolicy.REPLACER_MASK_SYMBOL);
        DesensitizedRule desensitizedRule = DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(2, 3);
        String original = "qweqweqwewqe";
        String result = desensitizedStrategy.convert(original, desensitizedRule);

        System.out.println(original.length());
        System.out.println(result.length());
        System.out.println(result);

        System.out.println();
        System.out.println();
        System.out.println();

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setAddress("撒哈拉沙漠撒哈拉沙漠撒哈拉沙漠");
        baseResponse.setPhone("18519188941");

        DesensitizedProcessor desensitizedProcessor = new DefaultDesensitizedProcessor();
        desensitizedProcessor.process(baseResponse);

        System.out.println(baseResponse);

        System.out.println();
        System.out.println();
        System.out.println();

        UserBO userBO = new UserBO();
        userBO.setUserName("UserBO-setUserName");

        SubAddressBO subAddressBO = new SubAddressBO();
        subAddressBO.setAddrId("SubAddressBO-setAddrId");
        subAddressBO.setLen(0);

        AddressBO addressBO = new AddressBO();
        addressBO.setAddrName("AddressBO-setAddrName");
        addressBO.setSubAddressBO(subAddressBO);

        userBO.getList().add(addressBO);

        userBO.getMap().put("userBO-Map", addressBO);

        desensitizedProcessor.process(userBO);
        System.out.println(userBO);
    }
}