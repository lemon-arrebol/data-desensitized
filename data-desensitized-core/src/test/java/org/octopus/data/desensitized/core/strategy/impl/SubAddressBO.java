package org.octopus.data.desensitized.core.strategy.impl;


import java.io.Serializable;
import lombok.ToString;
import org.octopus.data.desensitized.core.annotation.Desensitized;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

@ToString
public class SubAddressBO implements Serializable {

    @Desensitized(desensitizedType = DesensitizedType.CNAPS_CODE)
    private String addrId;
    private int len;

    public SubAddressBO() {
    }

    public SubAddressBO(String addrId, int len) {
        this.addrId = addrId;
        this.len = len;
    }

    public String getAddrId() {
        return addrId;
    }

    public void setAddrId(String addrId) {
        this.addrId = addrId;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }
}