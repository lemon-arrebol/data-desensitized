package org.octopus.data.desensitized.core.strategy.impl;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.octopus.data.desensitized.core.annotation.Desensitized;
import org.octopus.data.desensitized.core.builder.DataDesensitizedToStringBuilder;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

@Getter
@Setter
public class BaseResponse<T> {

    @Desensitized(desensitizedType = DesensitizedType.ADDRESS)
    private String address;

    @Desensitized(desensitizedType = DesensitizedType.MOBILE_PHONE)
    private String phone;

    private T data;

    public BaseResponse() {
    }

    @Override
    public String toString() {
        return new DataDesensitizedToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}