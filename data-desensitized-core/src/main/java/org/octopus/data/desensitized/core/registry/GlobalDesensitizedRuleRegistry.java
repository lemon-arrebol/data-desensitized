package org.octopus.data.desensitized.core.registry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.slf4j.Slf4j;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;
import org.octopus.data.desensitized.core.util.DesensitizedUtils;

/**
 * @author lemon
 * @date 2021/8/28 09:43
 */
@Slf4j
public class GlobalDesensitizedRuleRegistry {

    private static final Map<DesensitizedType, DesensitizedRule> DESENSITIZED_RULES =
            new ConcurrentHashMap<>(16);

    public static void registerDesensitizedRule(DesensitizedType type, DesensitizedRule desensitizedRule) {
        GlobalDesensitizedRuleRegistry.DESENSITIZED_RULES.put(type, desensitizedRule);
        log.info("注册脱敏规则[{}]，，默认规则信息：{}", type, desensitizedRule);
    }

    public static DesensitizedRule obtainDesensitizedRule(DesensitizedType type) {
        return GlobalDesensitizedRuleRegistry.DESENSITIZED_RULES.get(type);
    }

    static {
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.DEFAULT,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(0, 0));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.CHINESE_NAME,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(1, 1));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.ID_CARD,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(0, 4));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.FIXED_PHONE,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(0, 4));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.MOBILE_PHONE,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(3, 4));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.ADDRESS,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(6, 0));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.EMAIL,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(1, 0));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.BANK_CARD,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(6, 4));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.CNAPS_CODE,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(2, 0));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.PASSWORD,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(0, 0));
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(DesensitizedType.CAR_NUMBER,
                DesensitizedUtils.buildDefaultReplacePolicyDesensitizedRule(2, 1));
    }
}
