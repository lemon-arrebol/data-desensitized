package org.octopus.data.desensitized.core.strategy.impl;

import org.apache.commons.lang3.StringUtils;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.strategy.AbstractDesensitizedStrategy;
import org.octopus.data.desensitized.core.util.DesensitizedUtils;

/**
 * 随机数脱敏策略
 *
 * @author lemon
 * @date 2021/8/27 11:44
 */
public class ReplaceRandomNumberDesensitizedStrategy extends AbstractDesensitizedStrategy<String> {

    @Override
    public DesensitizedPolicy supportPolicy() {
        return DesensitizedPolicy.REPLACER_RANDOM_NUMBER;
    }

    @Override
    public String doConvert(String original, DesensitizedRule desensitizedRule) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        if (original.length() <= 2) {
            desensitizedRule.setReplacePrefixIgnoreLength(1);
            desensitizedRule.setReplaceSuffixIgnoreLength(0);
        }

        return DesensitizedUtils.replaceMaskSymbol(original, desensitizedRule);
    }
}
