package org.octopus.data.desensitized.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

/**
 * @author lemon
 * @date 2021/8/27 11:11
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface Desensitized {

    /**
     * 脱敏类型：默认不脱敏
     */
    DesensitizedType desensitizedType() default DesensitizedType.DEFAULT;

    /**
     * 脱敏策略：默认使用遮罩符替换
     */
    DesensitizedPolicy desensitizedPolicy() default DesensitizedPolicy.REPLACER_MASK_SYMBOL;

    /**
     * 是否开启脱敏,默认是
     */
    boolean enable() default true;

    /**
     * 替换策略：前置忽略长度
     */
    int replacePrefixIgnoreLength() default DesensitizedRule.DEFAULT_VALUE;

    /**
     * 替换策略：后置忽略长度
     */
    int replaceSuffixIgnoreLength() default DesensitizedRule.DEFAULT_VALUE;

    /**
     * 替换策略：遮罩符
     */
    String replaceMaskSymbol() default "*";

    /**
     * 整体替换值
     */
    String replaceOverallValue() default "";

    /**
     * 追加策略：起始索引
     */
    int appendStartIndex() default DesensitizedRule.DEFAULT_VALUE;

    /**
     * 追加策略：追加遮罩符
     */
    String appendMaskSymbol() default "*";

    /**
     * 追加策略：追加遮罩符数量
     */
    int appendMaskSymbolNumber() default DesensitizedRule.DEFAULT_VALUE;

    /**
     * 追加策略：追加指定值
     */
    String appendSpecialValue() default "";

    /**
     * 裁剪策略：起始索引
     */
    int cutStartIndex() default DesensitizedRule.DEFAULT_VALUE;

    /**
     * 裁剪策略：裁剪长度
     */
    int cutLength() default DesensitizedRule.DEFAULT_VALUE;
}
