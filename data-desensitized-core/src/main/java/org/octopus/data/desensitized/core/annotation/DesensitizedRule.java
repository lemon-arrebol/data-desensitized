package org.octopus.data.desensitized.core.annotation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

/**
 * @author lemon
 * @date 2021/8/27 11:51
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DesensitizedRule {

    public static final int DEFAULT_VALUE = -1;

    /**
     * 脱敏类型：默认脱敏所有值
     */
    DesensitizedType desensitizedType = DesensitizedType.DEFAULT;

    /**
     * 脱敏策略：默认使用遮罩符替换
     */
    DesensitizedPolicy desensitizedPolicy = DesensitizedPolicy.REPLACER_MASK_SYMBOL;

    /**
     * 是否开启脱敏,默认是
     */
    private boolean enable = true;

    /**
     * 替换策略：前置忽略长度
     */
    private int replacePrefixIgnoreLength = DEFAULT_VALUE;

    /**
     * 替换策略：后置忽略长度
     */
    private int replaceSuffixIgnoreLength = DEFAULT_VALUE;

    /**
     * 替换策略：遮罩符
     */
    private String replaceMaskSymbol = "*";

    /**
     * 整体替换值
     */
    String replaceOverallValue;

    /**
     * 追加策略：起始索引
     */
    int appendStartIndex = DEFAULT_VALUE;

    /**
     * 追加策略：追加遮罩符
     */
    String appendMaskSymbol = "*";

    /**
     * 追加策略：追加遮罩符数量
     */
    int appendMaskSymbolNumber = DEFAULT_VALUE;

    /**
     * 追加策略：追加指定值
     */
    String appendSpecialValue;

    /**
     * 裁剪策略：起始索引
     */
    int cutStartIndex = DEFAULT_VALUE;

    /**
     * 裁剪策略：裁剪长度
     */
    int cutLength = DEFAULT_VALUE;
}
