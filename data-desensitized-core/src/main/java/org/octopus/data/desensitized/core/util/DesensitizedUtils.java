package org.octopus.data.desensitized.core.util;

import org.apache.commons.lang3.StringUtils;
import org.octopus.data.desensitized.core.annotation.Desensitized;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.registry.GlobalDesensitizedRuleRegistry;

/**
 * 数据脱敏工具类
 */
public class DesensitizedUtils {

    /**
     * 默认替换策略脱敏规则
     *
     * @param replacePrefixIgnoreLength
     * @param replaceSuffixIgnoreLength
     * @return
     */
    public static DesensitizedRule buildDefaultReplacePolicyDesensitizedRule(int replacePrefixIgnoreLength,
            int replaceSuffixIgnoreLength) {
        return DesensitizedUtils.buildReplacePolicyDesensitizedRule(Boolean.TRUE, replacePrefixIgnoreLength,
                replaceSuffixIgnoreLength, null);
    }

    /**
     * @param enable
     * @param replacePrefixIgnoreLength
     * @param replaceSuffixIgnoreLength
     * @param maskSymbol
     * @return
     */
    public static DesensitizedRule buildReplacePolicyDesensitizedRule(boolean enable, int replacePrefixIgnoreLength,
            int replaceSuffixIgnoreLength, String maskSymbol) {
        if (replacePrefixIgnoreLength < 0) {
            replacePrefixIgnoreLength = 0;
        }

        if (replaceSuffixIgnoreLength < 0) {
            replaceSuffixIgnoreLength = 0;
        }

        DesensitizedRule desensitizedRule = new DesensitizedRule();
        desensitizedRule.setEnable(enable);
        desensitizedRule.setReplacePrefixIgnoreLength(replacePrefixIgnoreLength);
        desensitizedRule.setReplaceSuffixIgnoreLength(replaceSuffixIgnoreLength);

        if (StringUtils.isNotBlank(maskSymbol)) {
            desensitizedRule.setReplaceMaskSymbol(maskSymbol);
        }

        return desensitizedRule;
    }

    /**
     * @param desensitized
     * @return
     */
    public static DesensitizedRule buildDesensitizedRule(Desensitized desensitized) {
        return DesensitizedRule.builder()
                .desensitizedType(desensitized.desensitizedType())
                .desensitizedPolicy(desensitized.desensitizedPolicy())
                .enable(desensitized.enable())
                .replacePrefixIgnoreLength(desensitized.replacePrefixIgnoreLength())
                .replaceSuffixIgnoreLength(desensitized.replaceSuffixIgnoreLength())
                .replaceMaskSymbol(desensitized.replaceMaskSymbol())
                .replaceOverallValue(desensitized.replaceOverallValue())
                .appendStartIndex(desensitized.appendStartIndex())
                .appendMaskSymbol(desensitized.appendMaskSymbol())
                .appendMaskSymbolNumber(desensitized.appendMaskSymbolNumber())
                .appendSpecialValue(desensitized.appendSpecialValue())
                .cutStartIndex(desensitized.cutStartIndex())
                .cutLength(desensitized.cutLength())
                .build();
    }

    /**
     * @param original         原始数据
     * @param desensitizedRule 脱敏规则
     */
    public static String replaceMaskSymbol(String original, DesensitizedRule desensitizedRule) {
        // 应用默认规则
        DesensitizedUtils.useDefaultDesensitizedRule(desensitizedRule);
        return DesensitizedUtils.replaceMaskSymbol(original, desensitizedRule.getReplacePrefixIgnoreLength(),
                desensitizedRule.getReplaceSuffixIgnoreLength(), desensitizedRule.getReplaceMaskSymbol());
    }

    /**
     * 设置默认脱敏规则
     *
     * @param desensitizedRule
     */
    public static void useDefaultDesensitizedRule(DesensitizedRule desensitizedRule) {
        DesensitizedRule defaultDesensitizedRule =
                GlobalDesensitizedRuleRegistry.obtainDesensitizedRule(desensitizedRule.getDesensitizedType());

        if (defaultDesensitizedRule == null) {
            return;
        }

        if (desensitizedRule.getReplacePrefixIgnoreLength() == DesensitizedRule.DEFAULT_VALUE) {
            desensitizedRule.setReplacePrefixIgnoreLength(defaultDesensitizedRule.getReplacePrefixIgnoreLength());
        }

        if (desensitizedRule.getReplaceSuffixIgnoreLength() == DesensitizedRule.DEFAULT_VALUE) {
            desensitizedRule.setReplaceSuffixIgnoreLength(defaultDesensitizedRule.getReplaceSuffixIgnoreLength());
        }

        if (StringUtils.isBlank(desensitizedRule.getReplaceMaskSymbol())) {
            desensitizedRule.setReplaceMaskSymbol(defaultDesensitizedRule.getReplaceMaskSymbol());
        }

        if (StringUtils.isBlank(desensitizedRule.getReplaceOverallValue())) {
            desensitizedRule.setReplaceOverallValue(defaultDesensitizedRule.getReplaceOverallValue());
        }

        if (desensitizedRule.getAppendStartIndex() == DesensitizedRule.DEFAULT_VALUE) {
            desensitizedRule.setAppendStartIndex(defaultDesensitizedRule.getAppendStartIndex());
        }

        if (StringUtils.isBlank(desensitizedRule.getAppendMaskSymbol())) {
            desensitizedRule.setAppendMaskSymbol(defaultDesensitizedRule.getAppendMaskSymbol());
        }

        if (desensitizedRule.getAppendMaskSymbolNumber() == DesensitizedRule.DEFAULT_VALUE) {
            desensitizedRule.setAppendMaskSymbolNumber(defaultDesensitizedRule.getAppendMaskSymbolNumber());
        }

        if (StringUtils.isBlank(desensitizedRule.getAppendSpecialValue())) {
            desensitizedRule.setAppendSpecialValue(defaultDesensitizedRule.getAppendSpecialValue());
        }

        if (desensitizedRule.getCutStartIndex() == DesensitizedRule.DEFAULT_VALUE) {
            desensitizedRule.setCutStartIndex(defaultDesensitizedRule.getCutStartIndex());
        }

        if (desensitizedRule.getCutLength() == DesensitizedRule.DEFAULT_VALUE) {
            desensitizedRule.setCutLength(defaultDesensitizedRule.getCutLength());
        }
    }

    /**
     * @param original         原始数据
     * @param prefixNoMaskSize 开始N个字符不脱敏
     * @param suffixNoMaskSize 结尾N个字符不脱敏
     * @param maskFlag         脱敏字符串
     */
    public static String replaceMaskSymbol(String original, int prefixNoMaskSize, int suffixNoMaskSize,
            String maskFlag) {
        // 空不需要脱敏
        if (StringUtils.isBlank(original) || original.length() <= prefixNoMaskSize) {
            return original;
        }

        if (prefixNoMaskSize < 0) {
            prefixNoMaskSize = 0;
        }

        if (suffixNoMaskSize < 0) {
            suffixNoMaskSize = 0;
        }

        if (prefixNoMaskSize + suffixNoMaskSize >= original.length()) {
            return original;
        }

        String left = "";

        if (prefixNoMaskSize >= 1) {
            left = StringUtils.left(original, prefixNoMaskSize);
        }

        String right = "";

        if (suffixNoMaskSize >= 1) {
            right = StringUtils.right(original, suffixNoMaskSize);
        }

        original = StringUtils.rightPad(left, (original.length() - suffixNoMaskSize), maskFlag) + right;

        return original;
    }
}