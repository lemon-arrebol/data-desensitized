package org.octopus.data.desensitized.core.registry.impl;

import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.registry.DesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.registry.GlobalDesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.strategy.DesensitizedStrategy;

/**
 * @author lemon
 * @date 2021/8/27 11:42
 */
public class DefaultDesensitizedStrategyRegistry implements DesensitizedStrategyRegistry {

    @Override
    public <T> DesensitizedStrategy<T> obtainDesensitizedStrategy(Class<T> dataType, DesensitizedPolicy policy) {
        return GlobalDesensitizedStrategyRegistry.obtainDesensitizedStrategy(dataType, policy);
    }

    @Override
    public <T> void register(DesensitizedStrategy<T> desensitizedStrategy) {
        GlobalDesensitizedStrategyRegistry.registerDesensitizedStrategy(desensitizedStrategy);
    }
}
