package org.octopus.data.desensitized.core.strategy.impl;

import org.apache.commons.lang3.StringUtils;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.strategy.AbstractDesensitizedStrategy;

/**
 * 整体替换脱敏策略
 *
 * @author lemon
 * @date 2021/8/27 11:44
 */
public class ReplaceOverallDesensitizedStrategy extends AbstractDesensitizedStrategy<String> {

    @Override
    public DesensitizedPolicy supportPolicy() {
        return DesensitizedPolicy.REPLACER_OVERALL;
    }

    @Override
    public String doConvert(String original, DesensitizedRule desensitizedRule) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return desensitizedRule.getReplaceOverallValue();
    }
}
