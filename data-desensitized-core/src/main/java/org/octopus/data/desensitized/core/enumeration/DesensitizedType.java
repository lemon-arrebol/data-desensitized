package org.octopus.data.desensitized.core.enumeration;

/**
 * 脱敏类型：手机号、身份证号、地址等用户敏感信息，不同的类型对应不同的规则
 *
 * @author lemon
 * @date 2021/8/27 11:15
 */
public enum DesensitizedType {
    /**
     * 默认
     */
    DEFAULT,
    /**
     * 中文名
     */
    CHINESE_NAME,
    /**
     * 身份证号
     */
    ID_CARD,
    /**
     * 座机号
     */
    FIXED_PHONE,
    /**
     * 手机号
     */
    MOBILE_PHONE,
    /**
     * 地址
     */
    ADDRESS,
    /**
     * 电子邮件
     */
    EMAIL,
    /**
     * 银行卡
     */
    BANK_CARD,
    /**
     * 公司开户银行联号
     */
    CNAPS_CODE,
    /**
     * 密码
     */
    PASSWORD,
    /**
     * 车牌号
     */
    CAR_NUMBER;
}
