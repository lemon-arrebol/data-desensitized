package org.octopus.data.desensitized.core.registry;

import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;

/**
 * @author houjuntao
 * @date 2021/8/29 10:47
 */
public interface DesensitizedRuleRegistry {

    DesensitizedRule obtainDesensitizedRule(DesensitizedType type);

    void register(DesensitizedType type, DesensitizedRule desensitizedRule);
}
