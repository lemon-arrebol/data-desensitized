package org.octopus.data.desensitized.core.registry.impl;

import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;
import org.octopus.data.desensitized.core.registry.DesensitizedRuleRegistry;
import org.octopus.data.desensitized.core.registry.GlobalDesensitizedRuleRegistry;

/**
 * @author houjuntao
 * @date 2021/8/29 10:49
 */
public class DefaultDesensitizedRuleRegistry implements DesensitizedRuleRegistry {

    @Override
    public DesensitizedRule obtainDesensitizedRule(DesensitizedType type) {
        return GlobalDesensitizedRuleRegistry.obtainDesensitizedRule(type);
    }

    @Override
    public void register(DesensitizedType type, DesensitizedRule desensitizedRule) {
        GlobalDesensitizedRuleRegistry.registerDesensitizedRule(type, desensitizedRule);
    }
}
