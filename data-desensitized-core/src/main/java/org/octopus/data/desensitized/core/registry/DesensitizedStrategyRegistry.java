package org.octopus.data.desensitized.core.registry;

import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.strategy.DesensitizedStrategy;

/**
 * @author lemon
 * @date 2021/8/27 11:33
 */
public interface DesensitizedStrategyRegistry {

    <T> DesensitizedStrategy<T> obtainDesensitizedStrategy(Class<T> dataType, DesensitizedPolicy policy);

    <T> void register(DesensitizedStrategy<T> desensitizedStrategy);
}
