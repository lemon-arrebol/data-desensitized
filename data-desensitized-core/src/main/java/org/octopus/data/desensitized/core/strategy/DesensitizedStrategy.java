package org.octopus.data.desensitized.core.strategy;

import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;

/**
 * @author lemon
 * @date 2021/8/27 11:14
 */
public interface DesensitizedStrategy<T> {

    Class<? extends DesensitizedStrategy<?>> EMPTY_DESENSITIZED_STRATEGY = null;

    /**
     * 数据脱敏数据类型
     *
     * @return
     */
    Class<T> supportDataType();

    /**
     * 数据脱敏策略
     *
     * @return
     */
    DesensitizedPolicy supportPolicy();

    /**
     * 数据脱敏
     *
     * @param original
     * @param desensitizedRule
     * @return
     */
    T convert(T original, DesensitizedRule desensitizedRule);
}
