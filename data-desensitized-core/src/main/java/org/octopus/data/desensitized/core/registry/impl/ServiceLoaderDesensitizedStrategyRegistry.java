package org.octopus.data.desensitized.core.registry.impl;

import java.util.ServiceLoader;
import org.octopus.data.desensitized.core.strategy.DesensitizedStrategy;

/**
 * @author lemon
 * @date 2021/8/27 11:42
 */
public class ServiceLoaderDesensitizedStrategyRegistry extends DefaultDesensitizedStrategyRegistry {

    public ServiceLoaderDesensitizedStrategyRegistry() {
        ServiceLoader.load(DesensitizedStrategy.class).forEach(super::register);
    }
}
