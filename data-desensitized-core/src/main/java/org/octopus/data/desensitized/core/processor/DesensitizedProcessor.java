package org.octopus.data.desensitized.core.processor;

/**
 * @author lemon
 * @date 2021/8/27 12:40
 */
public interface DesensitizedProcessor {

    <T> T process(T object);
}
