package org.octopus.data.desensitized.core.enumeration;

/**
 * 脱敏策略：遮罩符替换(字符串)、随机数替换(字符串、数值)、整体替换(基础类型)、裁剪(基础类型)、追加(基础类型)、密文(字符串)
 *
 * @author lemon
 * @date 2021/8/27 11:15
 */
public enum DesensitizedPolicy {
    /**
     * 遮罩符替换
     */
    REPLACER_MASK_SYMBOL,
    /**
     * 随机数
     */
    REPLACER_RANDOM_NUMBER,
    /**
     * 整体替换
     */
    REPLACER_OVERALL,
    /**
     * 追加遮罩符
     */
    APPEND_MASK_SYMBOL,
    /**
     * 追加随机数
     */
    APPEND_RANDOM_NUMBER,
    /**
     * 追加指定值
     */
    APPEND_SPECIAL_VALUE,
    /**
     * 裁剪
     */
    CUTTING,
    /**
     * 加密
     */
    ENCRYPTION;
}
