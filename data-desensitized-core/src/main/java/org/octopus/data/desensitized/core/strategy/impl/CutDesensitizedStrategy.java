package org.octopus.data.desensitized.core.strategy.impl;

import org.apache.commons.lang3.StringUtils;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.strategy.AbstractDesensitizedStrategy;
import org.octopus.data.desensitized.core.util.DesensitizedUtils;

/**
 * @author lemon
 * @date 2021/8/27 11:44
 */
public class CutDesensitizedStrategy extends AbstractDesensitizedStrategy<String> {

    @Override
    public DesensitizedPolicy supportPolicy() {
        return DesensitizedPolicy.CUTTING;
    }

    @Override
    public String doConvert(String original, DesensitizedRule desensitizedRule) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return DesensitizedUtils.replaceMaskSymbol(original, desensitizedRule);
    }
}
