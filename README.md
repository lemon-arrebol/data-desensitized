# 数据脱敏
## 日志数据脱敏
    通常在日志中输出对象数据时，会有些敏感数据(比如：密码、手机号、住址等)信息直接输出，存在泄漏数据风险。可以通过重写对象的toString()方法，对敏感字段进行处理。  
一般有两种方案进行数据脱敏处理:  
1. toString 方法中指定特定字段避免输出  
* 使用lombok @ToString注解，指定字段不输出  
```
@ToString
public class DesensitizedDemo {
    @ToString.Exclude
    private String password;
}
```

* 使用commons-langs ToStringBuilder指定字段不输出
```
public class DesensitizedDemo {
    @ToStringExclude
    private String password;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
```

2. 对特定字段加部分进行遮罩, 比如内容替换为'*'
基于 @Desensitized 注解用于数据脱敏指定脱敏规则, 使用 DataDesensitizedToStringBuilder 来重写 toString() 方法.
DataDesensitizedToStringBuilder 底层使用的是 commons-langs ReflectionToStringBuilder.

* 指定脱敏规则
```
public class DesensitizedDemo {
    private String name;
    @Desensitized(replacePrefixIgnoreLength = 2, replaceSuffixIgnoreLength = 3, replaceMaskSymbol = "$")
    private String password;
    
    @Override
    public String toString() {
        return new DataDesensitizedToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}
```

* 使用默认脱敏规则
```
public class DesensitizedDemo {
    private String name;
    @Desensitized(desensitizedType = DesensitizedType.PASSWORD)
    private String password;
    
    @Override
    public String toString() {
        return new DataDesensitizedToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}
```

## 接口返回数据脱敏
用户在使用网站、App、小程序等应用时，不可避免的会留存一些用户个人敏感数据，比如手机号、身份证号、家庭住址、公司地址等敏感信息。
应用在存储用户敏感数据时可能是明文或密文存储，用户在使用应用过程中会访问到这些敏感数据，如果直接以明文方式返回，网络传输过程中可能
会被非法拦截到导致敏感数据泄漏。对于敏感信息用户可能并不会真正查看全部的原始明文，如果以密文方式返回会增加加解密的开销，
密文一般都比明文大也会增加网络传输开销，当敏感数据比较多时开销会剧增。敏感数据脱敏是安全性与性能之间权衡后的折中方案，
在应用返回数据前将敏感数据脱敏后返回给用户，脱敏后的数据和原始明文差不多一样大，没有增加网络开销也没有加解密的开销。

# 数据脱敏对象
数据脱敏是针对基础数据进行遮罩处理，将敏感数据指定部位替换成遮罩符号，一般是针对字符串数据进行脱敏处理。

# 数据脱敏类型
常见的用户敏感数据有手机号、证件号、银行卡号、地址、密码等，不同的敏感数据处理规则也不一样

# 数据脱敏规则(脱敏场景)
enable 是否开启脱敏
replacePrefixIgnoreLength 前置不需要打码的长度
replaceSuffixIgnoreLength 后置不需要打码的长度
replaceMaskSymbol 遮罩符，用什么字符或字符串代替原始字符
    
# 数据脱敏策略
除了上述提到的替换字符方式外，针对数字类型科室用随机数替换、裁剪原始数据、使用统一数字代替等，本质上都是替换字符，只不过替换的策略不同

1. 替换：遮罩符替换、随机数替换、统一值替换、统一值整体替换
2. 追加：指定位置追加指定值
3. 裁剪：剔除指定位置字符本质还是替换字符
4. 加密：数据加密处理

# 数据脱敏使用
* data-desensitized-core
定义脱敏注解、脱敏类型、脱敏策略等基础功能。
   * 日志输出对象数据脱敏：@Desensitized + DataDesensitizedToStringBuilder
   * 接口返回数据脱敏：@Desensitized + DesensitizedProcessor

* data-desensitized-fastJson
结合fastJson框架自定义序列化器FastJsonDesensitizedSerializer，在脱敏字段上使用fastJson注解@JSONField指定自定义序列化类，
定义@Desensitized注解指定脱敏规则，数据序列化时会自动进行脱敏。
```
@JSONField(serializeUsing = FastJsonDesensitizedSerializer.class)
@Desensitized(desensitizedType = DesensitizedType.ADDRESS)
```

* data-desensitized-jackson
结合jackson框架自定义序列化器JacksonDesensitizedSerializer，在脱敏字段上使用jackson注解@JsonSerialize指定序列化类，
定义@Desensitized注解指定脱敏规则，数据序列化时会自动进行脱敏
   * @JsonSerialize + @Desensitized
    ```
    @JsonSerialize(using = JacksonDesensitizedSerializer.class)
    @Desensitized(desensitizedType = DesensitizedType.ADDRESS)
    ```
   * @JacksonDesensitized
    注解@JacksonDesensitized组合了@JsonSerialize、@Desensitized两个注解
    ```
    @JacksonDesensitized(desensitizedType = DesensitizedType.MOBILE_PHONE)
    ```
