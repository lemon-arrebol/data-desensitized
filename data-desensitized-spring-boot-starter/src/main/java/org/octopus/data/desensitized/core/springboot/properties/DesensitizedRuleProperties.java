package org.octopus.data.desensitized.core.springboot.properties;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lemon
 * @date 2021/8/28 11:38
 */
@Data
@ConfigurationProperties(prefix = "org.octopus.data.desensitized")
public class DesensitizedRuleProperties {

    /**
     * key - 脱敏策略 {@link DesensitizedType}
     * value - 脱敏规则
     */
    Map<String, DesensitizedRule> rules = new HashMap<>();
}
