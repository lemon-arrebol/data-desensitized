package org.octopus.data.desensitized.core.springboot.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.octopus.data.desensitized.core.registry.DesensitizedRuleRegistry;
import org.octopus.data.desensitized.core.registry.DesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.registry.impl.DefaultDesensitizedRuleRegistry;
import org.octopus.data.desensitized.core.spring.registry.SpringDesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.springboot.properties.DesensitizedRuleProperties;
import org.octopus.data.desensitized.core.strategy.impl.ReplaceMaskSymbolDesensitizedStrategy;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lemon
 * @date 2021/8/28 11:35
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({DesensitizedRuleProperties.class})
public class DesensitizedConfiguration {

    @Bean
    public ReplaceMaskSymbolDesensitizedStrategy replaceMaskSymbolDesensitizedStrategy() {
        return new ReplaceMaskSymbolDesensitizedStrategy();
    }

    @Bean
    public DesensitizedStrategyRegistry desensitizedStrategyRegistry() {
        return new SpringDesensitizedStrategyRegistry();
    }

    @Bean
    public DesensitizedRuleRegistry desensitizedRuleRegistry(DesensitizedRuleProperties desensitizedRuleProperties) {
        DesensitizedRuleRegistry desensitizedRuleRegistry = new DefaultDesensitizedRuleRegistry();

        if (MapUtils.isNotEmpty(desensitizedRuleProperties.getRules())) {
            desensitizedRuleProperties.getRules().forEach((type, desensitizedRule) -> {
//                desensitizedRuleRegistry.register(type, desensitizedRule);
            });
        }

        return desensitizedRuleRegistry;
    }
}
