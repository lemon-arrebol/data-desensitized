package org.octopus.data.desensitized.core.springboot.config;


import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.octopus.data.desensitized.core.annotation.DesensitizedRule;
import org.octopus.data.desensitized.core.enumeration.DesensitizedPolicy;
import org.octopus.data.desensitized.core.enumeration.DesensitizedType;
import org.octopus.data.desensitized.core.registry.DesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.strategy.DesensitizedStrategy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author lemon
 * @date 2021/8/28 12:14
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DesensitizedConfiguration.class})
public class DesensitizedConfigurationTest {

    @Resource
    private DesensitizedConfiguration desensitizedConfiguration;

    @Resource
    private DesensitizedStrategyRegistry desensitizedStrategyRegistry;

    @Test
    public void setUp() throws Exception {
        System.out.println(desensitizedConfiguration);

        DesensitizedRule desensitizedRule = new DesensitizedRule();
        desensitizedRule.setDesensitizedType(DesensitizedType.CHINESE_NAME);
        desensitizedRule.setReplaceMaskSymbol("$#");

        DesensitizedStrategy<String> desensitizedStrategy =
                desensitizedStrategyRegistry.obtainDesensitizedStrategy(String.class,
                        DesensitizedPolicy.REPLACER_MASK_SYMBOL);

        System.out.println(desensitizedStrategy.convert("wqeqweqweqw", desensitizedRule));
    }
}