package org.octopus.data.desensitized.fastjson;

import com.alibaba.fastjson.JSONObject;
import org.octopus.data.desensitized.core.registry.DesensitizedStrategyRegistry;
import org.octopus.data.desensitized.core.registry.impl.ServiceLoaderDesensitizedStrategyRegistry;

/**
 * @author lemon
 * @date 2021/8/28 09:08
 */
public class FastJsonDesensitizedSerializerTest {

    public static void main(String[] args) {
        DesensitizedStrategyRegistry desensitizedStrategyRegistry = new ServiceLoaderDesensitizedStrategyRegistry();

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setAddress("BaseResponse-setAddress");
        baseResponse.setPhone("BaseResponse-setPhone");

        System.out.println(JSONObject.toJSONString(baseResponse));
    }
}